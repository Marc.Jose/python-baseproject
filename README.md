# Python Base-Project

## About This Project
This repository aims to provide a basic Python project. 
Included in the toolchain are several analysis tools which needs to be installed locally:

* [Pylint](https://www.pylint.org/)
* [MyPy](http://mypy-lang.org/)
* [PyTest](https://docs.pytest.org/en/latest/)

There exist also several configuration files for them in the root folder.

## How To Set Up
This project is already ready to use but given that you probably want to use 
your own structure, names and dependencies you should have a look and modify 
the following files:
* `setup.cfg`  
    Replace package information with your own ones following the template.  

## Build Project

### Python Environment
In order to have better control about your dependencies it is recommended to use
the virtualenv module and create an isolated Python environment:
```shell script
python3 -m venv /path/to/your/env
source /path/to/your/env/bin/activate
```

### Building and Installing
To build and install the project run:
```shell script
./setup.py build
./setup.py install
```
from within your previously created environment.

### Testing
Included in this template are libraries to run unit tests with coverage as well 
as a static type checker and linter. To execute them run:
```shell script
# Unittests with coverage
./setup.py --quiet test

# Static type checker
mypy --strict

# Linter
pylint src
```