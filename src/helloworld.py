#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Print "Hello World!" and exit.
"""


class HelloWorld:
    """
    HelloWorld object that stores a text field.
    """

    def __init__(self, text: str) -> None:
        self.__text = text

    def set_text(self, text: str) -> None:
        """
        Set the @text field.
        """
        self.__text = text

    @property
    def text(self) -> str:
        """
        Text property.
        """
        return self.__text
