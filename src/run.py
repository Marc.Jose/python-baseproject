#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Create HelloWorld object and print its text.
"""

from src.helloworld import HelloWorld


def main() -> None:
    """
    Main entry point.
    Creates a @HelloWorld object.
    """
    printer: HelloWorld = HelloWorld('Hello World!')
    print(printer.text)
